let a = prompt('Enter the first number');
let b = prompt('Enter the second number');
let c = prompt('Enter +, -, *, /.');
let num1 = checkNum(+a);
let num2 = checkNum(+b);
let action = checkAction(c);

function checkNum(num) {
    if (isNaN(+num) || +num == null) {
        while (isNaN(+num) || +num == null) {
            num = prompt('Enter the number!', num);
        }
    }
    return(num);
}

function checkAction(action) {

    if (action !== '*' && action !== '/' && action !== '+' && action !== '-') {
        while (action !== '*' && action !== '/' && action !== '+' && action !== '-') {
            action = prompt('Enter +, -, *, /.', action);
        }
    }
    return(action);
}

function calculate(num1, num2, action) {
    let result;
    switch (action) {
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
    }
    return(result);
}
console.log(`Your first number = ${num1}`);
console.log(`And second number = ${num2}`);
console.log(`Your action = ${action}`);
console.log(`Your result: ${calculate(num1, num2, action)}`);
